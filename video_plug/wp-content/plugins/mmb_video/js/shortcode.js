jQuery(document).ready(function($) {

    tinymce.create('tinymce.plugins.mmb_plugin', {
        init : function(ed, url) {
                // Register command for when button is clicked
                ed.addCommand('mmb_insert_shortcode', function() {
                    
                    //content =  '[prefix_video id="" border_color="#" border_width="px"]';
                    //tinymce.execCommand('mceInsertContent', false, content);
                     ed.windowManager.open({
                        title: 'Video shortcode',
                        body: [
                            {type: 'textbox', name: 'post_id', label: 'Post ID'},
                            {type: 'textbox', name: 'border_width', label: 'Border width'},
                            {type: 'colorpicker', name: 'border_color', label: 'Border color'}
                            
                        ],
                        onsubmit: function(e) {    
                            ed.focus();
                            ed.selection.setContent('[prefix_video id="'+ e.data.post_id+'" border_color="'+ e.data.border_color+'" border_width="'+ e.data.border_width+'"]');
                        }
                    });
                });

            // Register buttons - trigger above command when clicked
            ed.addButton('mmb_button', {title : 'Insert shortcode', cmd : 'mmb_insert_shortcode', image: url + '/tinymce_button.png' });
        },   
    });

    // first parameter is the button ID1
    // second parameter must match the first parameter of the tinymce.create() function above
    tinymce.PluginManager.add('mmb_button', tinymce.plugins.mmb_plugin);
});