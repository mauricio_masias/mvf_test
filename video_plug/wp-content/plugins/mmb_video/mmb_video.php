<?php
/*
Plugin Name: MMB Video
Description: Custom plugin create a custom post type called videos for MVF.
Version: 1.0.1
Author: Mauricio Masias
Author URI: http://masias.co.uk
Text Domain: mmb-video
License: GPLv2 or later
*/



define( 'MMB_PATH', __FILE__ );
define( 'CPT_PATH', '/wp-content/plugins/mmb_video/' ); 


//the post types
require_once dirname( __FILE__ ) . '/src/post_types.php';



// the main plugin class
require_once dirname( __FILE__ ) . '/src/main.php';
Mmb__Custom__Fields::instance();


//Limit plugin to User Role/capability
require_once dirname( __FILE__ ) . '/src/capability.php';


//Add shortcode rutines
require_once dirname( __FILE__ ) . '/src/shortcodes.php';

//Add template control
//Add scripts and styles to footer
require_once dirname( __FILE__ ) . '/src/templates_control.php';

//Add tinymce hooks
require_once dirname( __FILE__ ) . '/src/tinymce.php';


?>
