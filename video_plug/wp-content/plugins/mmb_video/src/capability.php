<?php


/*
* administrator
* editor
* author
* contributor
* subscriber
*/

function mmb_remove_menu_items() {
    if( current_user_can( 'author' ) ):
        remove_menu_page( 'edit.php?post_type=videos' );
    endif;
}
add_action( 'admin_menu', 'mmb_remove_menu_items');