<?php
/**
 * Main Tribe Events Calendar class.
 */

// Don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}




if ( !class_exists('Mmb__Custom__Fields') ) {
 
    class Mmb__Custom__Fields {
        /**
        * @var  string  $prefix  The prefix for storing custom fields in the postmeta table
        */
        var $prefix = '_mmb_';
        /**
        * @var  array  $postTypes  An array of public custom post types, plus the standard "post" and "page" - add the custom types you want to include here
        */
        var $postTypes = array( "videos" );
        /**
        * @var  array  $customFields  Defines the custom fields available
        */
        var $customFields = array();
        
        /**
         * The single instance of the plugin class
         *
         * @var object
         * @since 2.0.0
         */
        protected static $_instance = null;

         /**
         * Return the main plugin instance
         *
         * @return object
         * @since 2.0.0
         */
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
        * PHP 5 Constructor
        */
        function __construct() {
        	$this->customFields = $this->defineCustomFields();
            add_action( 'admin_menu', array( $this, 'createCustomFields' ) );
            add_action( 'save_post', array( $this, 'saveCustomFields' ), 1, 2 );
            // Comment this line out if you want to keep default custom fields meta box
            add_action( 'do_meta_boxes', array( $this, 'removeDefaultCustomFields' ), 10, 3 );
        }


        /**
        * ADD all required custom fields
        */
        function defineCustomFields(){

        	return array(
	            array(
	                "name"          => "title",
	                "title"         => __("Title",'mmb_theme'),
	                "description"   => "",
	                "type"          => "text",
	                "scope"         => $this->postTypes,
	                "capability"    => "edit_posts"
	            ),
	            array(
	                "name"          => "subtitle",
	                "title"         => __("Subtitle",'mmb_theme'),
	                "description"   => "",
	                "type"          => "text",
	                "scope"         => $this->postTypes,
	                "capability"    => "edit_posts"
	            ),
	            array(
	                "name"          => "description",
	                "title"         => __("Description",'mmb_theme'),
	                "description"   => "",
	                "type"          => "textarea",
	                "scope"         => $this->postTypes,
	                "capability"    => "edit_posts"
	            ),
	            array(
	                "name"          => "id",
	                "title"         => __("ID",'mmb_theme'),
	                "description"   => "",
	                "type"          => "text",
	                "scope"         =>  $this->postTypes,
	                "capability"    => "edit_posts"
	            ),
	            array(
	                "name"          => "type",
	                "title"         => __("Type",'mmb_theme'),
	                "description"   => "",
	                "type"          => "select",
	                "option"		=> array(__("YouTube",'mmb_theme'),__("Vimeo",'mmb_theme'),__("DailyMotion",'mmb_theme')),
	                "scope"         =>  $this->postTypes,
	                "capability"    => "edit_posts"
	            )
	        );
        }
        /**
        * Remove the default Custom Fields meta box
        */
        function removeDefaultCustomFields( $type, $context, $post ) {
            foreach ( array( 'normal', 'advanced', 'side' ) as $context ) {
                foreach ( $this->postTypes as $postType ) {
                    remove_meta_box( 'postcustom', $postType, $context );
                }
            }
        }
        /**
        * Create the new Custom Fields meta box
        */
        function createCustomFields() {
            if ( function_exists( 'add_meta_box' ) ) {
                foreach ( $this->postTypes as $postType ) {
                    add_meta_box( 'my-custom-fields', __('Video Fields','mmb_theme'), array( $this, 'displayCustomFields' ), $postType, 'normal', 'high' );
                }
            }
        }
        /**
        * Display the new Custom Fields meta box
        */
        function displayCustomFields() {
            global $post;
            
            echo '<div class="form-wrap">';
                
                wp_nonce_field( 'my-custom-fields', 'my-custom-fields_wpnonce', false, true );
                foreach ( $this->customFields as $customField ) {
                    // Check scope
                    $scope = $customField[ 'scope' ];
                    $output = false;
                    foreach ( $scope as $scopeItem ) {
                        switch ( $scopeItem ) {
                            default: {
                                if ( $post->post_type == $scopeItem )
                                    $output = true;
                                break;
                            }
                        }
                        if ( $output ) break;
                    }
                    // Check capability
                    if ( !current_user_can( $customField['capability'], $post->ID ) )
                        $output = false;
                    
                    if ( $output ) { ?>
                        <div class="form-field form-required">
                            <?php
                            switch ( $customField[ 'type' ] ) {
                                
                                case "checkbox": 
                                    // CHECKBOX
                                    echo '<label for="'.$this->prefix.$customField[ 'name' ].'"><b>'. $customField[ 'title' ].'</b></label>&nbsp;&nbsp;';
                                    echo '<input type="checkbox" name="'.$this->prefix.$customField['name'].'" id="'.$this->prefix . $customField['name'] . '" value="yes"';
                                    
                                    if ( get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == "yes" )
                                        echo ' checked="checked"';
                                    echo '" />';
                                	break;
                                

                                case "select":
                                    // SELECT
                                    echo '<label for="'.$this->prefix.$customField['name'].'" ><b>' . $customField[ 'title' ].'</b></label>&nbsp;&nbsp;';
                                    
                                    echo '<select name="'.$this->prefix.$customField[ 'name' ].'">';
                                    
                                    foreach($customField['option'] as $val){
                                    	echo '<option value="'.$val.'"';
                                    	if ( get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == $val ){
                                    		echo ' selected="selected"';
                                    	}
                                    	echo '>'.$val.'</option>';
                                    }
                                    echo '</select>';
									break;
                                
                                
                                case "textarea":
                                 
                                    // TEXTAREA
                                    echo '<label for="'.$this->prefix.$customField[ 'name' ].'"><b>'.$customField[ 'title' ] . '</b></label>';
                                    
                                    echo '<textarea name="'.$this->prefix.$customField[ 'name' ].'" id="'.$this->prefix.$customField[ 'name' ].'" columns="30" rows="3">'.htmlspecialchars( get_post_meta( $post->ID, $this->prefix.$customField[ 'name' ], true ) ).'</textarea>';
                               		 break;
                                

                                default: 
                                    // TEXT
                                    echo '<label for="'.$this->prefix.$customField[ 'name' ].'"><b>'.$customField[ 'title' ] . '</b></label>';
                                    
                                    echo '<input type="text" name="'.$this->prefix.$customField[ 'name' ].'" id="'.$this->prefix.$customField[ 'name' ].'" value="'.htmlspecialchars( get_post_meta( $post->ID, $this->prefix.$customField[ 'name' ], true ) ).'" />';
                                    break;
                                
                            }
                            ?>
                            <?php if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>'; ?>
                        </div>
                    <?php
                    }
                } 
             echo '</div>';
            
        }
        /**
        * Save the new Custom Fields values
        */
        function saveCustomFields( $post_id, $post ) {
            
            if ( !isset( $_POST[ 'my-custom-fields_wpnonce' ] ) || !wp_verify_nonce( $_POST[ 'my-custom-fields_wpnonce' ], 'my-custom-fields' ) )
                return;
            if ( !current_user_can( 'edit_post', $post_id ) )
                return;
            if ( ! in_array( $post->post_type, $this->postTypes ) )
                return;

            foreach ( $this->customFields as $customField ) {
                if ( current_user_can( $customField['capability'], $post_id ) ) {
                    if ( isset( $_POST[ $this->prefix . $customField['name'] ] ) && trim( $_POST[ $this->prefix . $customField['name'] ] ) ) {
                        $value = $_POST[ $this->prefix . $customField['name'] ];
                        
                        update_post_meta( $post_id, $this->prefix . $customField[ 'name' ], $value );
                    } else {
                        delete_post_meta( $post_id, $this->prefix . $customField[ 'name' ] );
                    }
                }
            }
        }
 
    } // End Class
 
} // End if class exists statement
 

