<?php

	
if ( ! function_exists( 'create_post_types' ) ) {

	add_action( 'init', 'create_post_types', 0 );
	function create_post_types() {

		// Declare a PostType class

		
			class PostType {

				public function __construct(

					$name,
					$slug,
					$hasArchive,
					$hasSinglePage,
					$labelSingle,
					$labelPlural,
					$menuIcon,
					$public,
					$hierarchical,
					$supports

				) {

					$this->name = $name;
					$this->slug = $slug;
					$this->hasArchive = $hasArchive;
					$this->hasSinglePage = $hasSinglePage;
					$this->labelSingle = $labelSingle;
					$this->labelPlural = $labelPlural;
					$this->menuIcon = $menuIcon;
					$this->public = $public;
					$this->hierarchical = $hierarchical;
					$this->supports = $supports;
					

				}

			}


			
			// Declare an array of post types
			// menu_position is handled in functions.php

			$postTypes[] = new PostType(

				'videos', // name
				'videos', // slug
				'videos', // has_archive: false|slug
				true,// has single page active
				'Video',  // single label
				'Videos', // plural label
				'dashicons-video-alt3', // menu icon - https://developer.wordpress.org/resource/dashicons/#wordpress
				true, // public
				false, // hierarchical: if true, remember to use 'page-attributes' in support
				array( 'title', 'editor', 'custom-fields' )
				// supports: title, editor, author, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats

			);

	

			// Loop through and register the post types

			foreach ($postTypes as $postType) {

				register_post_type( $postType->name, array(

					'labels'             => array(

						'name'               => $postType->labelPlural,
						'singular_name'      => $postType->labelSingle,
						'add_new'            => 'Add New',
						'add_new_item'       => 'Add New '. $postType->labelSingle,
						'edit_item'          => 'Edit '. $postType->labelSingle,
						'new_item'           => 'New '. $postType->labelSingle,
						'all_items'          => 'All '. $postType->labelPlural,
						'view_item'          => 'View '. $postType->labelSingle,
						'search_items'       => 'Search '. $postType->labelPlural,
						'not_found'          => 'No '. strtolower($postType->labelPlural) .' found',
						'not_found_in_trash' => 'No '. strtolower($postType->labelPlural) .' found in Trash',
						'parent_item_colon'  => '',
						'menu_name'          => $postType->labelPlural

					),
					'public'             => $postType->public,
					'publicly_queryable' => $postType->hasSinglePage,
					'show_ui'            => true,
					'show_in_menu'       => true,
					'menu_icon'          => $postType->menuIcon,
					'query_var'          => true,
					'rewrite'            => array( 'slug' => $postType->slug ),
					'capability_type'    => 'post',
					'has_archive'        => $postType->hasArchive,
					'hierarchical'       => $postType->hierarchical,
					'menu_position'      => null,
					'supports'           => $postType->supports,
					

				));

			}
			
			
	

	}
	
}	
