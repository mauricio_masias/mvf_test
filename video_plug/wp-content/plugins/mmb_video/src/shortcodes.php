<?php



function single_video_builder( $atts ) {
	

	

	$atts = shortcode_atts(array(

								'id' => '100',
								'border_color' => '#333',
								'border_width'=>'8px'

								), $atts, 'single_video_builder' );
	
	

	$args = array(
		'posts_per_page'   => 1,
		'p'				   => $atts['id'],
		'order'		   	   => 'ASC',
		'post_type'        => 'videos',
		'post_status'      => 'publish',
		'suppress_filters' => false
	);
	$vid = new WP_Query( $args );
	

	if($vid->found_posts > 0 ){

		include dirname(MMB_PATH) . '/templates/single_video.php';
			
	}else{echo '<p>No video found</p>';}	
		

}

add_shortcode( 'prefix_video', 'single_video_builder' );





