<?php
/**
 * The template for displaying video archive pages
 *
 */

get_header(); 

$args = array(
		'posts_per_page'   => -1,
		'order'		   	   => 'ASC',
		'orderby'		   => 'date',
		'post_type'        => 'videos',
		'post_status'      => 'publish',
		'suppress_filters' => false
	);
$vids = new WP_Query( $args );


?>

<div class="wrap videos">

	<?php if ( have_posts() ) : ?>
		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header><!-- .page-header -->
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php

		if($vids->found_posts > 0 ){

			while ( $vids->have_posts() ) : $vids->the_post(); $custom = get_post_custom();

				$title = $custom['_mmb_title'][0];
				$subtitle = $custom['_mmb_subtitle'][0];
				$description = $custom['_mmb_description'][0];
				$video_id = $custom['_mmb_id'][0];
				$video_type = $custom['_mmb_type'][0];
				
				switch($video_type){

					case 'YouTube':
									$pop ='popup-youtube';
									$pop_link = 'http://www.youtube.com/watch?v='.$video_id;
									$thumb = 'http://img.youtube.com/vi/'.$video_id.'/default.jpg';
									break;
					case 'DailyMotion':

									$pop ='popup-vimeo';
									$pop_link = 'http://www.dailymotion.com/embed/video/'.$video_id;
									$thumb = 'http://www.dailymotion.com/thumbnail/video/'.$video_id;
									break;
									 
									
					case 'Vimeo':
									$pop ='popup-vimeo';
									$pop_link = 'https://vimeo.com/'.$video_id;
									$json = file_get_contents('http://vimeo.com/api/v2/video/225726864.json');
									$obj = json_decode($json);
									$thumb = $obj[0]->thumbnail_medium;
									break;
				}

				?>
				<article>
					<div class="thumb">
						<a class="<?php echo $pop;?>" href="<?php echo $pop_link;?>">
							<img src="<?php echo $thumb;?>" alt="<?php echo $title;?>">
						</a>
					</div>
					<div class="info">
						<div class="title"><?php echo $title;?></div>
						<div class="subtitle"><?php echo $subtitle;?></div>
						<div class="description"><?php echo $description;?></div>
					</div>
				</article>

			<?php endwhile;

		}
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
