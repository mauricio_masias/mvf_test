<div class="videoWrapper">

	<?php while ( $vid->have_posts() ) : $vid->the_post(); $custom = get_post_custom();

		$title = $custom['_mmb_title'][0];
		$subtitle = $custom['_mmb_subtitle'][0];
		$description = $custom['_mmb_description'][0];
		$video_id = $custom['_mmb_id'][0];
		$video_type = $custom['_mmb_type'][0];
		$style = 'style="border:'.$atts['border_width'].' solid '.$atts['border_color'].'"';
		
		switch($video_type){

			case 'YouTube': ?>
				 <iframe width="560" height="349" src="http://www.youtube.com/embed/<?php echo $video_id;?>?rel=0&hd=1" frameborder="0" allowfullscreen <?php echo $style;?> ></iframe>
			
			<?php break;
			
			case 'DailyMotion': ?>
				<iframe frameborder="0" width="560" height="349" src="//www.dailymotion.com/embed/video/<?php echo $video_id;?>" allowfullscreen <?php echo $style;?> ></iframe>
			<?php break;
			
			case 'Vimeo': ?>
			
				<iframe src="https://player.vimeo.com/video/<?php echo $video_id;?>?color=f6f8f9" width="560" height="349" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen <?php echo $style;?> ></iframe>

			<?php break;
		}

		?>

	<?php endwhile; ?>
    
   
</div>



			


		
		
		

		



			


			
		
		
		

		